<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <title>新規投稿画面</title>
    </head>
    <body>
        <div class="main-contents">

            <!--test属性で条件を指定してtrueだったら以降を処理する-->
            <c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
				    <ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
				         	<li><c:out value="${errorMessage}" />
				        </c:forEach>
				     </ul>
				</div>

				<!--errorMessageをssesionから外す処理。
			    バリデーション処理などでエラーメッセージをセッションに保持した場合、
			    画面遷移してもメッセージが残ってしまうとき必要になる-->
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="header">
	                <a href="./">ホーム</a>
            </div>

			<div class="form-area">
			    <form action="message" method="post"><br />

	                <label for="title">件名（30文字以下）</label>
	                <input name="title" id="title" value="${message.title}"/> <br />

	                <label for="category">カテゴリ（10文字以下）</label>
	                <input name="category" id="category" value="${message.category}" /> <br />

	                <label for="text">投稿内容（1000文字以下）</label><br />
	                <textarea name="text" cols="100" rows="5" id="text"><c:out value="${message.text}"></c:out></textarea><br />

					<input type="submit" value="投稿" /> <br />

            	</form>
			</div>

            <div class="copyright"> Copyright(c)Nakajima Kana</div>
        </div>
    </body>
</html>