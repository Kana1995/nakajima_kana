<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <title>ユーザー管理</title>
    </head>
    <body>
       <div class="main-contents">
			<!--test属性で条件を指定してtrueだったら以降を処理する-->
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <div class="home">
	                <a href="./">ホーム</a>
            </div>

            <div class="message">
	                <a href="signup">ユーザー新規登録</a>
            </div>

		    <!-- 1ユーザー表示 (ユーザーの数だけ繰り返す)-->
				<div class="management">

						<!-- ユーザー一覧を表示 -->
				   		<c:forEach items="${users}" var="user">

					        	<label for="account">アカウント名</label>
					        	<c:out value="${user.account}" /><br />

					        	<label for="name">名前</label>
					        	<c:out value="${user.name}" /><br />

					            <label for="branchId">支社</label>
					            <c:out value="${user.branchName}" /><br />

					            <label for="departmentId">部署</label>
					            <c:out value="${user.departmentName}" /><br />

							<form action="setting" method="get"><br />
					            <input name="id" id="id" value="${user.id}" type="hidden"/>
								<input type="submit" value="編集">
							</form>

							<c:if test="${ loginUser.id != user.id }">
								<form action="stop" method="post">
									<c:if test="${ user.isStopped == 0 }" >
						                <input name="isStopped" id="isStopped" type="hidden" value= "1" />
						                <input name="id" id="id" value="${user.id}" type="hidden"/>
						                <input onclick="return confirm('本当に停止しますか？')" type="submit" value="停止" />
						            </c:if>

						            <c:if test="${ user.isStopped == 1 }">
						                <input name="isStopped" id="isStopped" type="hidden" value= "0"/>
						                <input name="id" id="id" value="${user.id}" type="hidden"/>
						                <input onclick="return confirm('本当に復活しますか？')" type="submit" value="復活" />
						            </c:if>

						            <br />
					            </form>
				            </c:if>

				    	</c:forEach>

				</div>

            <div class="copyright">Copyright(c)Nakajima Kana</div>
        </div>
    </body>
</html>