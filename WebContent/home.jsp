<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <title>掲示板システム</title>
    </head>

    <body>
    	<!--test属性で条件を指定してtrueだったら以降を処理する-->
    	<c:if test="${ not empty loginUser }">
			<div class="errorMessages">
			    <ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
			         	<li><c:out value="${errorMessage}" />
			        </c:forEach>
			     </ul>
			</div>
			<!--errorMessageをssesionから外す処理。
			    バリデーション処理などでエラーメッセージをセッションに保持した場合、
			    画面遷移してもメッセージが残ってしまうとき必要になる。（エラーメッセージ削除）-->
			<c:remove var="errorMessages" scope="session" />
		</c:if>

        <div class="main-contents">
   			<a href="message">新規投稿</a>

   			<c:if test="${ loginUser.branchId == 1 && loginUser.departmentId == 1 }">
   				<a href="management">ユーザー管理</a>
   			</c:if>

			<a href="logout">ログアウト</a>
        </div>


		<div class="narrowing-down">
			<form action="./" method="get"><br />
		  			<label for="date">日付</label>
				<input type="date" name="startDate" id="startDate" value="${startDate}"  />
				<input type="date" name="endDate" id="endDate" value="${endDate}"  />

				<label for="categorySearch">カテゴリ</label>
				<input name="categorySearch" id="categorySearch" value="${categorySearch}"  />

				<input type="submit" value="絞り込み" /> <br />
			</form>
        </div>


		<!-- 1投稿表示 (投稿数の数だけ繰り返す)-->
		<div class="messages">
		    <c:forEach items="${messages}" var="message">
		    	<!-- 投稿表示 -->
		    	<h1>Message</h1>
		        <div class="message">
		            <div class="name"><c:out value="${message.name}" /></div>
		            <div class="title"><c:out value="${message.title}" /></div>
		            <div class="category"><c:out value="${message.category}" /></div>
		            <pre class="text"><c:out value="${message.text}" /></pre>
		            <div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

					<!-- 投稿削除ボタン -->
					<!-- 投稿のmessageのuserIdがログインユーザーのidと一致したときに削除ボタンを表示 -->
				    <c:if test="${loginUser.id == message.userId}">
					<div class="delete-area">
			        	<form action="deleteMessage" method="post" >
			        	<input name="messageId" value="${message.id}" id="messageId" type="hidden"/>
			        	<input onclick="return confirm('本当に削除しますか？')" type="submit" value="削除" /> <br />
			        	</form>
		        	</div>
		        	</c:if>

		        </div>

				<!-- コメント表示 -->
				<h1>Comment</h1>
				<c:forEach items="${comments}" var="comment">
					<!-- コメントのmessageIdがmessageのidと一致したときにコメントを表示 -->
					<c:if test="${comment.messageId == message.id}">
						<div class="comment">
				        	<div class="name"><c:out value="${comment.name}" /></div>
				            <pre class="text"><c:out value="${comment.text}" /> </pre>
				            <div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			        	</div>

		        	<!-- コメント削除ボタン -->
		        	<!-- コメントのcommentのuserIdがログインユーザーのidと一致したときに削除ボタンを表示 -->
				    <c:if test="${loginUser.id == comment.userId}">
					<div class="delete-area">
			        	<form action="deleteComment" method="post" >
			        	<input name="commentId" value="${comment.id}" id="commentId" type="hidden"/>
			        	<input onclick="return confirm('本当に削除しますか？')" type="submit" value="削除" /> <br />
			        	</form>
		        	</div>
		        	</c:if>
		         </c:if>
			   </c:forEach>

				<!-- コメント入力 -->
				<div class="form-area">
					<form action="comment" method="post"><br />
						<input name="messageId" value="${message.id}" id="messageId" type="hidden"/>
			            <label for="text">コメント入力欄</label><br />
		                <textarea name="text" cols="100" rows="5" id="text"></textarea>
						<br />
						<input type="submit" value="コメント投稿" /> <br />
					</form>
		        </div>

		    </c:forEach>
		</div>

        <div class="copyright"> Copyright(c)Nakajima Kana</div>
    </body>
</html>