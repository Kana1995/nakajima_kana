<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    <title>ユーザー新規登録</title>
    </head>
    <body>
        <div class="main-contents">

			<!--test属性で条件を指定してtrueだったら以降を処理する-->
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <!--errorMessageをssesionから外す処理。
		    	バリデーション処理などでエラーメッセージをセッションに保持した場合、
		    	画面遷移してもメッセージが残ってしまうとき必要になる-->
				<c:remove var="errorMessages" scope="session" />
            </c:if>

			<a href="management">ユーザー管理</a> <br />

            <form action="signup" method="post"><br />
                <label for="account">アカウント名(半角英数字[azAZ0*9]で6文字以上20文字以下)</label>
                <input name="account" id="account" value="${user.account}" /> <br />

                <label for="password">パスワード(記号を含む全ての半角文字で6文字以上20文字以下)</label>
                <input name="password" type="password" id="password" /> <br />

				<label for="passwordConfirmation">確認用パスワード</label>
                <input name="passwordConfirmation" type="password" id="passwordConfirmation" /> <br />

				<label for="name">名前(10文字以下)</label>
				<input name="name" id="name" value="${user.name}" /> <br />

                <label for="branch">支社</label>
	                <select name="branchId" id="branchId">
	                <c:forEach items="${branches}" var="branch">
	                	<c:if test="${ branch.id == user.branchId }">
							<option value="${branch.id}" selected><c:out value="${branch.name}" /></option>
						</c:if>
						<c:if test="${ branch.id != user.branchId }">
							<option value="${branch.id}" ><c:out value="${branch.name}" /></option>
						</c:if>
					</c:forEach>
					</select>
                <br />

                <label for="department">部署</label>
	                <select name="departmentId" id="departmentId">
	                <c:forEach items="${departments}" var="department">
						<c:if test="${ department.id == user.departmentId }">
							<option value="${department.id}" selected><c:out value="${department.name}" /></option>
						</c:if>
						<c:if test="${ department.id != user.departmentId }">
							<option value="${department.id}" ><c:out value="${department.name}" /></option>
						</c:if>
					</c:forEach>
					</select>
                <br />

                <input type="submit" value="登録" /> <br />
            </form>

            <div class="copyright">Copyright(c)Nakajima Kana</div>
        </div>
    </body>
</html>