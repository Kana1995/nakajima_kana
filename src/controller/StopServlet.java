package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        //リクエストの情報（User情報）を取得してuserへ代入
        User user = getUser(request);

        //updateメソッドでUserServiceのuserを更新
        new UserService().update(user.getIsStopped(), user.getId());

        response.sendRedirect("./management");
    }

    //User型のgetUserメソッド
    private User getUser(HttpServletRequest request) throws IOException, ServletException {

    	//リクエストの情報をuserにセットする
        User user = new User();

        //値を指定してパラメータを取得
        user.setIsStopped(Integer.parseInt(request.getParameter("isStopped")));
        user.setId(Integer.parseInt(request.getParameter("id")));

        return user;
    }
}