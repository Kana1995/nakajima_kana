package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//ServletからJSPを表示させる
        request.getRequestDispatcher("message.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	// セッションを取得する
        HttpSession session = request.getSession();

        //エラーメッセージをListとして宣言
        List<String> errorMessages = new ArrayList<String>();

        //リクエストの情報（Message情報）を取得してmessageへ代入
        Message message = getMessage(request);

        if (!isValid(message, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            //各入力値を保持（エラーメッセージと同じように画面に返す）
            request.setAttribute("message", message);
            request.getRequestDispatcher("message.jsp").forward(request, response);
            return;
        }

        //セッションスコープのuserのloginUserを取得
        User user = (User) session.getAttribute("loginUser");
        //userのuserIdをmessageへセット
        message.setUserId(user.getId());

        //正常な値だった場合はUserServiceのmessageへ挿入
        new MessageService().insert(message);
        //別のWebページ（UserService）を呼び出す
        response.sendRedirect("./");
    }

    //Message型のgetMessageメソッド
    private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

    	//リクエストの情報をmessageにセットする
        Message message = new Message();

        //値を指定してパラメータを取得
        message.setTitle(request.getParameter("title"));
        message.setText(request.getParameter("text"));
        message.setCategory(request.getParameter("category"));
        return message;
    }

    //投稿に対するバリデーション
    private boolean isValid(Message message, List<String> errorMessages) {

    	//message（リクエスト）から値を取得
        String title = message.getTitle();
        String text  = message.getText();
        String category = message.getCategory();

    	if (StringUtils.isBlank(title)) {
            errorMessages.add("件名を入力してください");
        } else if (30 < title.length()) {
            errorMessages.add("件名は30文字以下で入力してください");
        }

        if (StringUtils.isBlank(text)) {
            errorMessages.add("本文を入力してください");
        } else if (1000 < text.length()) {
            errorMessages.add("本文は1000文字以下で入力してください");
        }

        if (StringUtils.isBlank(category)) {
            errorMessages.add("カテゴリを入力してください");
        } else if (10 < category.length()) {
            errorMessages.add("カテゴリは10文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}