package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

//URLパターンを設定
@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//支店名一覧をListとして宣言
        List<Branch> branches = new BranchService().select();

        //部署名一覧をListとして宣言
        List<Department> departments = new DepartmentService().select();

        //リクエスト領域にデータを格納
        request.setAttribute("branches", branches);
        request.setAttribute("departments", departments);

    	//ServletからJSPを表示させる
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//エラーメッセージをListとして宣言
        List<String> errorMessages = new ArrayList<String>();

        //支店名一覧をListとして宣言
        List<Branch> branches = new BranchService().select();

        //部署名一覧をListとして宣言
        List<Department> departments = new DepartmentService().select();

        //リクエストの情報（User情報）を取得してuserへ代入
        User user = getUser(request);

        //確認用パスワードをバリデーションするために宣言
        String passwordConfirmation = request.getParameter("passwordConfirmation");
		//バリデーションを実行
        if (!isValid(user, passwordConfirmation, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            //アカウントの入力値を保持（エラーメッセージと同じように画面に返す）
        	request.setAttribute("user", user);
        	//リクエスト領域にデータを格納
            request.setAttribute("branches", branches);
            request.setAttribute("departments", departments);
            request.getRequestDispatcher("signup.jsp").forward(request, response);

            return;
        }

        //正常な値だった場合はUserServiceのuserへ挿入
        new UserService().insert(user);

        //別のWebページ（UserService）を呼び出す
        response.sendRedirect("./management");
    }

    //User型のgetUserメソッド
    private User getUser(HttpServletRequest request) throws IOException, ServletException {

    	//リクエストの情報をuserにセットする
        User user = new User();

        //"name"を指定してパラメータを取得
        //URLに「name="(requestで受け取ったデータ)"」の「name」
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

        return user;
    }

    //ユーザー情報に対するバリデーション
    private boolean isValid(User user,String passwordConfirmation, List<String> errorMessages) {

    	//user（リクエスト）から値を取得
        String account = user.getAccount();
        String password = user.getPassword();
        String name = user.getName();
        int branchId =user.getBranchId();
        int departmentId =user.getDepartmentId();

        //アカウント名のバリデーション
        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if (!(account.matches("^[azAZ0*9]")) && !(6 < account.length() && account.length() < 20)){
            errorMessages.add("アカウント名は半角英数字で6文字以上、20文字以下で入力してください");

        //アカウントが重複していたらエラー文を表示
        } else {
        	User userSignUp = new UserService().select(account);

        	if(userSignUp != null) {
        		errorMessages.add("該当のアカウント名はすでに存在しています");
        	}
        }

        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        } else if (!(password.matches("^[-_@+*;:#$%&A-Za-z0-9]{6,20}$"))){
            errorMessages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
        }

        //パスワードとパスワード確認用に入力された値が等しくない場合
        if (!(password.equals(passwordConfirmation))){
            errorMessages.add("パスワードと確認用パスワードが一致しません");
        }

        if (StringUtils.isEmpty(passwordConfirmation)) {
            errorMessages.add("確認用パスワードを入力してください");
        }

        //名前のバリデーション
        if (StringUtils.isEmpty(name)) {
            errorMessages.add("名前を入力してください");
        } else if (!(name.length() < 10)){
            errorMessages.add("名前は10文字以下で入力してください");
        }

        //支店と役職の組み合わせが不正の場合
        if (!(branchId == 1 && departmentId == 1 || branchId == 1  && departmentId == 2 ||
        	  branchId == 2 && departmentId == 3 || branchId == 2  && departmentId == 4 ||
        	  branchId == 3 && departmentId == 3 || branchId == 3  && departmentId == 4 ||
        	  branchId == 4 && departmentId == 3 || branchId == 4  && departmentId == 4 )){
            errorMessages.add("支店と役職の組み合わせが不正です");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}