package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	// セッションを取得する
        HttpSession session = request.getSession();

        //エラーメッセージをListとして宣言
        List<String> errorMessages = new ArrayList<String>();

        //リクエストの情報（Comment情報）を取得してcommentへ代入
        Comment comment = getComment(request);

        if (!isValid(comment, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
        }

        //セッションスコープのuserのloginUserを取得
        User user = (User) session.getAttribute("loginUser");
        //userのuserIdをcommentへセット
        comment.setUserId(user.getId());

        //正常な値だった場合はUserServiceのuserへ挿入
        new CommentService().insert(comment);
        //別のWebページ（UserService）を呼び出す
        response.sendRedirect("./");
    }

    //Comment型のgetCommentメソッド
    private Comment getComment(HttpServletRequest request) throws IOException, ServletException {

    	//リクエストの情報をcommentにセットする
        Comment comment = new Comment();

        comment.setText(request.getParameter("text"));

        //JSPからmessage.idを取得
        comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
        return comment;
    }

    private boolean isValid(Comment comment, List<String> errorMessages) {

    	//comment（リクエスト）から値を取得
        String text  = comment.getText();

    	if (StringUtils.isBlank(text)) {
            errorMessages.add("コメントを入力してください");
        } else if (500 < text.length()) {
            errorMessages.add("500文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}