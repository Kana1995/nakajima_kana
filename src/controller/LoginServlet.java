package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

//URLパターンを設定
@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//処理中のページから別のページへ処理を移す
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//エラーメッセージをListとして宣言
        List<String> errorMessages = new ArrayList<String>();

        //リクエストの情報（User情報）を取得してuserへ代入
        User user = getUser(request);

		//バリデーションを実行
        if (!isValid(user, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            //アカウントの入力値を保持（エラーメッセージと同じように画面に返す）
        	request.setAttribute("account", request.getParameter("account"));
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

		User userConfirmation = new UserService().select(user.getAccount(), user.getPassword());
        if (userConfirmation == null || userConfirmation.getIsStopped() == 1) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
			request.setAttribute("errorMessages", errorMessages);
			//アカウントの入力値を保持（エラーメッセージと同じように画面に返す）
        	request.setAttribute("account", request.getParameter("account"));
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

        HttpSession session = request.getSession();
        session.setAttribute("loginUser", userConfirmation);
		//処理中のページから別のページへ処理を移す
		response.sendRedirect("./");
    }

	//User型のgetUserメソッド
    private User getUser(HttpServletRequest request) throws IOException, ServletException {

    	//リクエストの情報をuserにセットする
        User user = new User();

        //値を指定してパラメータを取得
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        return user;
    }

    //ユーザー情報に対するバリデーション
    private boolean isValid(User user, List<String> errorMessages) {

    	//user（リクエスト）から値を取得
        String account = user.getAccount();
  		String password = user.getPassword();

        //アカウント名のバリデーション
        if (StringUtils.isBlank(account)) {
            errorMessages.add("アカウントが入力されていません");
        }

        //パスワードのバリデーション
        if (StringUtils.isBlank(password)) {
            errorMessages.add("パスワードが入力されていません");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}