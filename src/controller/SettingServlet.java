package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

//URLパターンを設定
@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//支店名一覧をListとして宣言
        List<Branch> branches = new BranchService().select();

        //部署名一覧をListとして宣言
        List<Department> departments = new DepartmentService().select();

        //リクエストの情報をuserにセットする
        User user = new User();

        //エラーメッセージをListとして宣言
        List<String> errorMessages = new ArrayList<String>();

        //URLのリクエストパラメータからid（String型）を取得
        String parameterId = request.getParameter("id");

        //URLパラメータに対するバリデーション
        if((StringUtils.isBlank(parameterId)) || !parameterId.matches("^[0-9]+$")) {
        	//idが空欄の場合 //idが数字以外が入った場合
            errorMessages.add("不正なパラメータが入力されました");
            request.setAttribute("errorMessages", errorMessages);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/management");
            dispatcher.forward(request, response);
            return;
        }

    	int userId = Integer.parseInt(parameterId);
    	User userConfirmation = new UserService().select(userId);

    	//idが既存のidではない場合
        if(userConfirmation == null) {
            errorMessages.add("不正なパラメータが入力されました");
            request.setAttribute("errorMessages", errorMessages);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/management");
            dispatcher.forward(request, response);
            return;
        }


        //リクエストスコープへuser,branch,departmentデータを格納
        request.setAttribute("user", user);
        request.setAttribute("branches", branches);
        request.setAttribute("departments", departments);

    	//ServletからJSPを表示させる
        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//エラーメッセージをListとして宣言
        List<String> errorMessages = new ArrayList<String>();

        //支店名一覧をListとして宣言
        List<Branch> branches = new BranchService().select();

        //部署名一覧をListとして宣言
        List<Department> departments = new DepartmentService().select();

        //リクエストの情報（User情報）を取得してuserへ代入
        User user = getUser(request);

        //確認用パスワードをバリデーションするために宣言
        String passwordConfirmation = request.getParameter("passwordConfirmation");
		//バリデーションを実行
        if (!isValid(user, passwordConfirmation, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
        	//リクエスト領域にデータを格納
            request.setAttribute("user", user);
            request.setAttribute("branches", branches);
            request.setAttribute("departments", departments);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
            return;
        }

    	//updateメソッドでUserServiceのuserを更新
        new UserService().update(user);

        response.sendRedirect("./management");
    }

    //User型のgetUserメソッド
    private User getUser(HttpServletRequest request) throws IOException, ServletException {

    	//ログインユーザーが自分のデータを編集したらセッション上のユーザーも更新
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");
        User user = new User();
        //ユーザー管理画面で選択したユーザーのidを取得
        user.setId(Integer.parseInt(request.getParameter("id")));

        //ユーザー編集画面で入力した値を取得
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));

        if(loginUser.getId() == user.getId()) {
        	 user.setBranchId(loginUser.getBranchId());
             user.setDepartmentId(loginUser.getDepartmentId());
        } else {
        	//ユーザー編集画で入力した値を取得
            user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
            user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
        }
        return user;
    }

    //ユーザー情報に対するバリデーション
    private boolean isValid(User user,String passwordConfirmation, List<String> errorMessages) {

    	//user（リクエスト）から値を取得
        String account = user.getAccount();
        String password = user.getPassword();
        String name = user.getName();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

        //アカウント名のバリデーション
        if (StringUtils.isBlank(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if(!StringUtils.isEmpty(account) && !(account.matches("^[azAZ0*9]{6,20}$"))){
            errorMessages.add("アカウント名は半角英数字で6文字以上、20文字以下で入力してください");

        //アカウントが重複していたらエラー文を表示
        } else {
    		//UserServiceのUserに引数としてaccountを追加
            User userAccount = new UserService().select(account);

            //アカウントが新しくないか＆アカウント名を変更したときに既存のアカウント名があるか
            if(userAccount != null &&  userAccount.getId() != user.getId()) {
            	errorMessages.add("該当のアカウント名はすでに存在しています");
        	}
        }

        if (!StringUtils.isEmpty(password) &&!(password.matches("^[-_@+*;:#$%&A-Za-z0-9]{6,20}$"))){
            errorMessages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力して下さい");
        }

        //パスワードとパスワード確認用に入力された値が等しくない場合
        if (!(password.equals(passwordConfirmation))){
            errorMessages.add("パスワードと確認用パスワードが一致しません");
        }

        //名前のバリデーション
        if (StringUtils.isBlank(name)) {
            errorMessages.add("名前を入力してください");
        } else if (!StringUtils.isEmpty(name) &&!(name.length() < 10)){
            errorMessages.add("名前は10文字以下で入力してください");
        }

        //支店と役職の組み合わせが不正の場合
        if (branchId == 1 && departmentId  > 2  || branchId != 1  && departmentId < 3 ) {
            errorMessages.add("支店と役職の組み合わせが不正です");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}