package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Message;
import service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		//リクエストの情報（Message情報）を取得してmessageへ代入
		Message message = getMessage(request);

		//正常な値だった場合はUserServiceのmessageへ挿入
		new MessageService().delete(message);
		//別のWebページ（UserService）を呼び出す
		response.sendRedirect("./");
    }

    //Message型のgetMessageメソッド
    private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

    	//リクエストの情報をmessageにセットする
        Message message = new Message();

        //値を指定してパラメータを取得
        message.setId(Integer.parseInt(request.getParameter("messageId")));
        return message;
    }
}