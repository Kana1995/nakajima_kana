package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

//URLパターンを設定
@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

	//doGetメソッドをオーバーライド
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//開始日時と終了日時（年月まで）をMessageServletへ引数として渡す
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String categorySearch = request.getParameter("categorySearch");

        //投稿一覧をListとして宣言
        List<UserMessage> messages = new MessageService().select(startDate, endDate, categorySearch);

        //コメント一覧をListとして宣言
        List<UserComment> comments = new CommentService().select();

        //リクエスト領域にデータを格納
        request.setAttribute("comments", comments);
        request.setAttribute("messages", messages);
        request.setAttribute("startDate", startDate);
        request.setAttribute("endDate", endDate);
        request.setAttribute("categorySearch", categorySearch);
        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }
}