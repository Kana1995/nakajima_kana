package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBranchDepartment;
import service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//UserServiceからuserを検索してきて、ユーザー一覧をListとして宣言
        List<UserBranchDepartment> users = new UserService().select();

        //リクエスト領域にデータを格納
        request.setAttribute("users", users);
        request.getRequestDispatcher("management.jsp").forward(request, response);
    }

}