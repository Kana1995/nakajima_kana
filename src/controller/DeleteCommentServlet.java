package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment" })
public class DeleteCommentServlet extends HttpServlet {

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		//リクエストの情報（Comment情報）を取得してcommentへ代入
        Comment comment = getComment(request);

		//正常な値だった場合はCommentServiceのcommentへ挿入
		new CommentService().delete(comment);
		//別のWebページ（CommentService）を呼び出す
		response.sendRedirect("./");
    }

    //Comment型のgetCommentメソッド
    private Comment getComment(HttpServletRequest request) throws IOException, ServletException {

    	//リクエストの情報をcommentにセットする
        Comment comment = new Comment();

        //値を指定してパラメータを取得
        comment.setId(Integer.parseInt(request.getParameter("commentId")));
        return comment;
    }
}