package beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

	//フィールド変数（メソッドではなくてクラスで持っている変数）
    private int id;
    private String title;
    private String text;
    private String category;
    private int userId;
    private Date createdDate;
    private Date updatedDate;

    //各getter/setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUptedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}