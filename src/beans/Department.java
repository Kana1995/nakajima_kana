package beans;

import java.io.Serializable;
import java.util.Date;

public class Department implements Serializable {

	//フィールド変数（メソッドではなくてクラスで持っている変数）
    private int id;
    private String name;
    private Date createdDate;
    private Date updatedDate;

    //各getter/setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}