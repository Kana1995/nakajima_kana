package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

public FilterConfig filterConfig;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
		      FilterChain chain) throws IOException, ServletException {

		//ServletRequest型のrequestとresponseをキャストでHttpServletRequest型に変換
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;

		// セッションスコープからユーザー情報を取得
        HttpSession session = req.getSession();
        User loginUser = (User) session.getAttribute("loginUser");

        String servletPath = req.getServletPath();

        // セッションがNullならば、ログイン画面へ飛ばす
        if(loginUser == null){

        	//urlが/loginの時
        	if(servletPath.equals("/login") ) {

        		// セッションがNullじゃなければサーブレットを実行
                chain.doFilter(req, res);

        	} else {
	        	// セッションがNullかつログインページ以外ならば、ログイン画面へ飛ばす

        		//エラーメッセージをListとして宣言
                List<String> errorMessages = new ArrayList<String>();
                errorMessages.add("ログインしてください");
        		req.setAttribute("errorMessages", errorMessages);
	            RequestDispatcher dispatcher = req.getRequestDispatcher("/login");
	            dispatcher.forward(req, res);
        	}

        } else {

        	// セッションがNullじゃなければサーブレットを実行
            chain.doFilter(request, response);
        }
    }

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}