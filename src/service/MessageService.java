package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	//MessageDaoへmessagesのデータを入れる
    public void insert(Message message) {

    	//接続を表すConnectionオブジェクトを初期化
        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //MessageDaoからmessagesの一覧データを取得
    public List<UserMessage> select(String startDate, String endDate, String categorySearch) {

    	//接続を表すConnectionオブジェクトを初期化
        Connection connection = null;
        try {
            connection = getConnection();

            String startDateData;
            String endDateData;

	          //開始日時を用意
	          if(!StringUtils.isEmpty(startDate)){
	          	startDateData = startDate + " 00:00:00" ;

	          } else {
	          	startDateData = "2020-01-01 00:00:00";
	          }

	          //終了日時を用意
	          if(!StringUtils.isEmpty(endDate) ) {
	          	endDateData = endDate + " 23:59:59" ;
	          } else {
	          	// 現在日時を取得
	      		Date date = new Date();

	      		// 表示形式を指定
	      		SimpleDateFormat sdformat
	      		= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	      		endDateData = sdformat.format(date);
	          }

	          List<UserMessage> messages = new UserMessageDao().select(connection, startDateData, endDateData, categorySearch);
			  commit(connection);
			  return messages;

    	} catch (RuntimeException e) {
        rollback(connection);
        throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //投稿削除機能
    public void delete(Message message) {

    	//接続を表すConnectionオブジェクトを初期化
        Connection connection = null;

        try {
            connection = getConnection();
            new MessageDao().delete(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}