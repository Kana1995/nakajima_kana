package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import beans.UserBranchDepartment;
import dao.UserBranchDepartmentDao;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

    public void insert(User user) {

    	//接続を表すConnectionオブジェクトを初期化
        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            connection = getConnection();
            //正常な値だった場合はUserDaoのuserへ挿入
            new UserDao().insert(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User select(String account ,String password) {

        Connection connection = null;
        try {
            // パスワード暗号化
        	String encPassword = CipherUtil.encrypt(password);

            connection = getConnection();
            //UserDaoのuserへアカウント名と暗号化したパスワードを挿入
            User user = new UserDao().select(connection, account ,encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserBranchDepartment> select() {

        Connection connection = null;
        try {
            connection = getConnection();

            List<UserBranchDepartment> users = new UserBranchDepartmentDao().select(connection);
            commit(connection);

            return users;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User select(String account) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, account);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(User user) {

    	Connection connection = null;
        try {
        	//パスワードnullじゃない場合に暗号化
        	if (!user.getPassword().isEmpty()) {
	            String encPassword = CipherUtil.encrypt(user.getPassword());
			    	user.setPassword(encPassword);
		    }
		    	connection = getConnection();
		    	//UserDaoのuserを更新
		        new UserDao().update(connection, user);

		        commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(int isStopped, int id) {

    	Connection connection = null;
        try {
            connection = getConnection();
            new UserDao().update(connection, isStopped, id);
            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User select(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();
            //UserDaoのuserへuserIDも挿入
            User user = new UserDao().select(connection, userId);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}