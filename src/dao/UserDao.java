package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {
    public void insert(Connection connection, User user) {

    	//PreparedStatementで利用するSQL文には、?(バインド変数)を入れて後で値を代入できるようにする。
        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();

            //StringBuilderオブジェクトに対してSQLで文字列や他のデータ型の値を追加
            sql.append("INSERT INTO users ( ");
            sql.append("    account, ");
            sql.append("    password, ");
            sql.append("    name, ");
            sql.append("    branch_id, ");
            sql.append("    department_id, ");
            sql.append("    is_stopped, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // account
            sql.append("    ?, ");                                  // password
            sql.append("    ?, ");                                  // name
            sql.append("    ?, ");                                  // branch_id
            sql.append("    ?, ");                                  // department_id
            sql.append("    ?, ");                                  // is_stopped
            sql.append("    CURRENT_TIMESTAMP, ");      // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

            //PreparedStatementオブジェクトの取得
            ps = connection.prepareStatement(sql.toString());

            //バインド変数に値を設定
            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getDepartmentId());
            ps.setInt(6, user.getIsStopped());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User select(Connection connection, String account, String encPassword) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

            //PreparedStatementオブジェクトの取得
            ps = connection.prepareStatement(sql);

            //バインド変数に値を設定
            ps.setString(1, account);
            ps.setString(2, encPassword);

            //"executeQuery"メソッドを実行して
            //データベースから何らかの結果が帰ってくる場合には
            //「ResultSet」インターフェースのオブジェクトとして取得
            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User select(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

            //PreparedStatementオブジェクトの取得
            ps = connection.prepareStatement(sql);

            //バインド変数に値を設定
            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());

            //"executeQuery"メソッドを実行して
            //データベースから何らかの結果が帰ってくる場合には
            //「ResultSet」インターフェースのオブジェクトとして取得
            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    /*
     * String型のaccountを引数にもつ、selectメソッドを追加する
     */
    public User select(Connection connection, String account) {

      PreparedStatement ps = null;
      try {

    	//新規のアカウント名が入ってくる
        String sql = "SELECT * FROM users WHERE account = ?";

        //PreparedStatementオブジェクトの取得
        ps = connection.prepareStatement(sql);
        //バインド変数に値を設定
        ps.setString(1, account);

        //SQLにSelect文を実行
        ResultSet rs = ps.executeQuery();

        List<User> users = toUsers(rs);
        if (users.isEmpty()) {
          return null;
        } else if (2 <= users.size()) {
          throw new IllegalStateException("ユーザーが重複しています");
        } else {
          return users.get(0);
        }
      } catch (SQLException e) {
        throw new SQLRuntimeException(e);
      } finally {
        close(ps);
      }
    }

    private List<User> toUsers(ResultSet rs) throws SQLException {

        List<User> users = new ArrayList<User>();
        try {
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setAccount(rs.getString("account"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));
                user.setBranchId(rs.getInt("branch_id"));
                user.setDepartmentId(rs.getInt("department_id"));
                user.setIsStopped(rs.getInt("is_stopped"));
                user.setCreatedDate(rs.getTimestamp("created_date"));
                user.setUpdatedDate(rs.getTimestamp("updated_date"));

                users.add(user);
            }
            return users;
        } finally {
            close(rs);
        }
    }

    public User select(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET ");
            sql.append("    account = ?, ");

            if(!user.getPassword().isEmpty()) {
            	sql.append("    password = ?, ");
            }
            sql.append("    name = ?, ");
            sql.append("    branch_id = ?, ");
            sql.append("    department_id = ?, ");
            sql.append("    is_stopped = ?, ");
            sql.append("    updated_date = CURRENT_TIMESTAMP ");

            sql.append("WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());

            if (!user.getPassword().isEmpty()) {
            	ps.setString(2, user.getPassword());
            	ps.setString(3, user.getName());
                ps.setInt(4, user.getBranchId());
                ps.setInt(5, user.getDepartmentId());
                ps.setInt(6, user.getIsStopped());
                ps.setInt(7, user.getId());
            }else {
            	ps.setString(2, user.getName());
                ps.setInt(3, user.getBranchId());
                ps.setInt(4, user.getDepartmentId());
                ps.setInt(5, user.getIsStopped());
                ps.setInt(6, user.getId());
            }

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void update(Connection connection, int isStopped, int id) {

    	PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
        	sql.append("UPDATE users SET ");
            sql.append("    is_stopped = ?, ");
            sql.append("    updated_date = CURRENT_TIMESTAMP ");

            sql.append("WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            //バインド変数に値を設定
            ps.setInt(1, isStopped);
            ps.setInt(2, id);

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


}