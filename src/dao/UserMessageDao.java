package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> select(Connection connection, String startDateData, String endDateData, String categorySearch) {

        PreparedStatement ps = null;

        try {
        	StringBuilder sql = new StringBuilder();

            sql.append("SELECT ");
            sql.append("    messages.id as id, ");
            sql.append("    messages.title as title, ");
            sql.append("    messages.text as text, ");
            sql.append("    messages.category as category, ");
            sql.append("    messages.user_id as user_id, ");
            sql.append("    users.name as name, ");
            sql.append("    messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_date BETWEEN ? AND ? ");

            if(!StringUtils.isEmpty(categorySearch)) {
	            sql.append("  	AND category LIKE  ?  ");
            }

            sql.append("  	ORDER BY created_date DESC" );

            ps = connection.prepareStatement(sql.toString());

            //WHEREに開始日時(2020-01-01 00:00:00)と終了日時（現在の日時）を入力

            ps.setString(1, startDateData);
            ps.setString(2, endDateData);

            if(!StringUtils.isEmpty(categorySearch)) {
                ps.setString(3, "%" + categorySearch +"%");
            }

            //SQL文を実行
            ResultSet rs = ps.executeQuery();

            List<UserMessage> messages = toUserMessages(rs);

            return messages;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

        List<UserMessage> messages = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                UserMessage message = new UserMessage();
                message.setId(rs.getInt("id"));
                message.setName(rs.getString("name"));
                message.setTitle(rs.getString("title"));
                message.setText(rs.getString("text"));
                message.setCategory(rs.getString("category"));
                message.setUserId(rs.getInt("user_id"));
                message.setCreatedDate(rs.getTimestamp("created_date"));

                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }
}